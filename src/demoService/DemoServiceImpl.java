package demoService;

import collectionsService.CollectionsServiceImpl;
import collectionsService.CollectionsService;
import collectionsService.CollectionsServiceStream;

import java.util.*;

public class DemoServiceImpl implements DemoService {

    /* Необходимо выбрать в текст (длинной не менее 1000 символов). В данном тексте необходимо:
    Подсчитать количество повторений для каждого слова - (принять, что знаки пробела,
    знаки препинания являются разделителями слов). Рекомендуется использовать Map<String, Long>.
    Вывести все уникальные слова из текста. Рекомендуется использовать Set<String>.
    Отсортировать все слова из текста (Подсказка: лучше и проще использовать Comparator).
    */

    private List<String> arrayList;

    @Override
    public void startDemo() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите \"1\" для использования модуля без Stream API и любое другое для использования модуля со Stream API");
        int i = 2;
        if (scanner.hasNextInt()) {
            i = scanner.nextInt();
        }

        CollectionsService collectionsService;

        if (i == 1) {

            System.out.println("ВЫБРАН ВАРИАНТ БЕЗ ИСПОЛЬЗОВАНИЯ Stream API");
            collectionsService = new CollectionsServiceImpl();

        } else {

            System.out.println("ВЫБРАН ВАРИАНТ С ИСПОЛЬЗОВАНИЕМ Stream API");
            collectionsService = new CollectionsServiceStream();

        }

        arrayList = collectionsService.readFromFile("TextForParsing.txt");

        collectionsService.countRepetitions(arrayList);

        collectionsService.printUniqueWords(arrayList);

        collectionsService.sortWords(arrayList);
    }

}
