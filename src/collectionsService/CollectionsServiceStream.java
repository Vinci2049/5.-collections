package collectionsService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class CollectionsServiceStream implements CollectionsService {

    @Override
    public List<String> readFromFile(String fileName) {

        List<String> arrayList = new ArrayList<>();

        String pattern = "(,| |!|-|–|«|»|\\r|\\n|\\r\\n|\\.|\\?)";
        try {
            arrayList = Files.lines(Paths.get(fileName))
                    .flatMap(s -> Stream.of(s.split(pattern)))
                    .filter(s -> s.length() > 0)
                    .collect(Collectors.toList());
        } catch (Exception e) {

            e.printStackTrace();
        }
        return arrayList;
    }

    /* Подсчитать количество повторений для каждого слова - (принять, что знаки пробела,
    знаки препинания являются разделителями слов). Рекомендуется использовать Map<String, Long>. */
    @Override
    public void countRepetitions(List<String> arrayList) {

        System.out.println("СЛОВА С КОЛИЧЕСТВОМ ПОВТОРЕНИЙ: ");

        arrayList.stream().collect(toMap(s -> s, s -> 1L, Long::sum))
                .forEach((s, aLong) -> System.out.println("Слово \""+s + "\" встречается в тексте " + aLong+ " раз"));

        System.out.println();
    }

    /* Вывести все уникальные слова из текста. Рекомендуется использовать Set<String>. */
    @Override
    public void printUniqueWords(List<String> arrayList) {

        System.out.println("УНИКАЛЬНЫЕ СЛОВА:");

        new HashSet<>(arrayList)
                .forEach(System.out::println);

        System.out.println();
    }

    /* Отсортировать все слова из текста (Подсказка: лучше и проще использовать Comparator). */
    @Override
    public void sortWords(List<String> arrayList) {
        System.out.println("ОТСОРТИРУЕМ СЛОВА ПО ДЛИНЕ: ");

        class LengthComparator implements Comparator<String> {
            public int compare(String first, String second) {
                return first.length() - second.length();
            }
        }

        Comparator<String> comparator = new LengthComparator();

        arrayList.stream().sorted(comparator).forEachOrdered(System.out::println);

    }
}
