package collectionsService;

import java.util.List;

public interface CollectionsService {

    List<String> readFromFile(String fileName);

    void countRepetitions(List<String> arrayList);

    void printUniqueWords(List<String> arrayList);

    void sortWords(List<String> arrayList);

}
