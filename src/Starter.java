import demoService.DemoService;
import demoService.DemoServiceImpl;

public class Starter {

    public static void main(String[] args) {
        DemoService demoService = new DemoServiceImpl();
        demoService.startDemo();
    }
}
