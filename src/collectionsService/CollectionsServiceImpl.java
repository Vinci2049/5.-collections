package collectionsService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.util.*;

public class CollectionsServiceImpl implements CollectionsService {

    @Override
    public List<String> readFromFile(String fileName) {

        List<String> arrayList = new ArrayList<>();
        try (Reader reader = new FileReader(fileName)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            StringBuilder stringBuilder = new StringBuilder();
            String ls = System.getProperty("line.separator");
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            stringBuilder.deleteCharAt(stringBuilder.length() - 1);

            String pattern = "(,| |!|-|–|«|»|\\r|\\n|\\r\\n|\\.|\\?)";

            String[] strings = stringBuilder.toString().split(pattern);

            for (String string : strings) {
                if (string.length() > 0) {
                    arrayList.add(string);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
        return arrayList;

    }

    /* Подсчитать количество повторений для каждого слова - (принять, что знаки пробела,
    знаки препинания являются разделителями слов). Рекомендуется использовать Map<String, Long>. */
    @Override
    public void countRepetitions(List<String> arrayList) {

        System.out.println("СЛОВА С КОЛИЧЕСТВОМ ПОВТОРЕНИЙ: ");
        Map<String, Long> map = new HashMap<>();

        arrayList.forEach(s -> map.merge(s, 1L, Long::sum));

        for (Map.Entry entry : map.entrySet()) {
            System.out.println("Слово \"" + entry.getKey() + "\" встречается в тексте " + entry.getValue()+" раз");
        }
        System.out.println();
    }


    /* Вывести все уникальные слова из текста. Рекомендуется использовать Set<String>. */
    @Override
    public void printUniqueWords(List<String> arrayList) {

        System.out.println("УНИКАЛЬНЫЕ СЛОВА:");
        Set<String> set = new HashSet<>(arrayList);

        for (String element : set) {
            System.out.println(element);
        }
        System.out.println();
    }


    /* Отсортировать все слова из текста (Подсказка: лучше и проще использовать Comparator). */
    @Override
    public void sortWords(List<String> arrayList) {

        System.out.println("ОТСОРТИРУЕМ СЛОВА ПО ДЛИНЕ: ");
        class LengthComparator implements Comparator<String> {
            public int compare(String first, String second) {
                return first.length() - second.length();
            }
        }

        Comparator<String> comparator = new LengthComparator();

        arrayList.sort(comparator);

        for (String element : arrayList) {
            System.out.println(element);
        }

    }

}
